import asyncio
import msgpack_yomo
import time
from dirhash import dirhash
from urllib.request import urlopen

async def demo_srv(*args, **kwargs):
    print(f'{args=} {kwargs=}')
    url = f'http://178.128.181.56:8080/?t={time.asctime()}'.replace(' ', '-')
    print(url)
    try:
        with urlopen(url) as response:
            print(response.status)
    except Exception as x:
        print(f'http exception: {x}')
    try:
        cmd_args = ['docker', 'ps', '-a']
        p = await asyncio.create_subprocess_exec(*cmd_args)
        if await p.wait():
            print('docker ps -a -- failed.')
    except Exception as x:
        print(f'subprocess exception: {x}')
    print(dirhash)
    print(msgpack_yomo)

