from srv.asset_engine.procs.procs_yn import ExpectOutput, ProcDir, ProcInput, proc_config, run_proc
from srv.asset_engine.asset_engine_core.asset_method_defs import asset_endecoder

async def start_local_code(code, input, proc_dir, name):
    if proc_dir is None:
        proc_dir0 = ProcDir.from_env(endecoder=asset_endecoder)
        proc_dir  = proc_dir0.new_proc_dir(name=name)
    return  await run_proc(
        proc_dir = proc_dir,
        cmds     = [
            ProcInput(obj=input),
            proc_config(python_snippets=[code]),
            ExpectOutput(early=True),
        ],
    )

