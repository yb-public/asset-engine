from typing import Any
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.tools.exceptions import CatchExceptions

@asset_class
class RemoteObjProxy:
    _remotes: callable
    _srv_url: str
    _key:     Any

    def __getattr__ (self, method):
        async def wrapper(**kwargs):
            request = RemoteMethodRequest(key=self._key, method=method, kwargs=kwargs)
            return await self._remotes(self._srv_url, request)
        return wrapper

@asset_class
class RemoteMethodRequest:
    key: Any
    method: str
    kwargs: dict

@asset_class
class RemoteObjHub:
    objects: dict
    endecoder: Any

    async def bin_to_bin(self, bin):
        with CatchExceptions():
            request = self.endecoder.bin_to_obj(bin)
            reply   = await self.request_to_reply(request)
            return self.endecoder.obj_to_bin(reply)

    async def request_to_reply(self, request):
        assert request.key in self.objects, f'RemoteObj no such key: {request.key}'
        obj = self.objects[request.key]
        return await getattr(obj, request.method)(**request.kwargs)

