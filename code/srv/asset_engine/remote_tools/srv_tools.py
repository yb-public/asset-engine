import falcon
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.tools.exceptions import CatchExceptions
from typing import Any

@asset_class
class FalconRoute:
    bin_to_bin: callable

    async def on_post(self, req, resp):
        with CatchExceptions():
            resp.status       = falcon.HTTP_200
            resp.content_type = falcon.MEDIA_MSGPACK
            resp.data         = await self.bin_to_bin(await req.stream.read())

    async def on_get(self, req, resp):
        resp.text = 'The Server is running.  Use POST requests.'

@asset_class
class StartupNotification:
    run: Any

    async def process_startup(self, scope, event):
        await self.run()

