import httpx
from typing import Any
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from functools import cached_property

@asset_class
class ConnectionPool:
    endecoder: Any
    default_timeout = 4  # Allow one additional second for round-trip.

    @cached_property
    def httpx_client(self):
        return httpx.AsyncClient()

    async def __call__(self, srv_url, request):
        try:
            content = self.endecoder.obj_to_bin(request)
            r = await self.httpx_client.post(url=srv_url, content=content)
            r.raise_for_status()
            return self.endecoder.bin_to_obj(r.content)
        except KeyboardInterrupt: raise
        except Exception as exception:
            raise ConnReqError(exception=exception)

    async def close_pool(self):
        await self.httpx_client.aclose()
        del self.httpx_client

@asset_class
class ConnReqError:
    exception: Any

