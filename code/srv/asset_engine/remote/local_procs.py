from textwrap import dedent
from srv.asset_engine.remote_tools.start_local_srv import start_local_code

async def start_local_asset_srv (host, port, route, proc_dir=None):
    #@require-async:asset_srv_main
    code = dedent("""
        from asset_engine.remote.asset_server import asset_srv_main
        asset_srv_main()
    """)
    name, input = 'asset_srv', [host, port, route]
    return await start_local_code(code=code, input=input, proc_dir=proc_dir, name=name)

async def start_local_worker (srv_url, queue_id, worker_id, proc_dir=None):
    #@require-async:asset_worker_main
    code = dedent("""
        from asyncio import run
        from asset_engine.remote.asset_worker import asset_worker_main
        run(asset_worker_main())
    """)
    name, input = 'asset_worker', [srv_url, queue_id, worker_id]
    return await start_local_code(code=code, input=input, proc_dir=proc_dir, name=name)

