from typing import Any, Optional
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class

@asset_class
class ResultData:
    result_bin: bytes
    error:      Any

@asset_class
class AssetReply:
    spec_known:  bool
    result_data: ResultData

@asset_class
class WorkReply:
    hash:          Optional[bytes] = None
    comp_spec_bin: Optional[bytes] = None
    comp_opts:     Optional[Any]   = None
    got_result:    bool            = False
    stop_worker:   bool            = False

