from typing import Any
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class

@asset_class
class RemoteAssets:
    asset_srv: Any

    async def cache_fn(self, cache_access):
        hash, account = cache_access.std_hash, cache_access.asset_engine.account
        timeout = self.asset_srv._remotes.default_timeout
        spec_known = False
        while True:
            if spec_known: 
                res = await self.asset_srv.get_asset(
                    account=account, hash=hash,
                    comp_spec_bin=None, comp_opts=None,
                    timeout=timeout,
                )
            else:
                res = await self.asset_srv.get_asset(
                    account=account, hash=hash, 
                    comp_spec_bin=cache_access.std_bin, comp_opts=cache_access.comp_opts,
                    timeout=timeout,
                )
            if res.result_data:
                if res.result_data.error:
                    raise RemoteAssetException(res.result_data.error)
                else:
                    endecoder = self.asset_srv._remotes.endecoder
                    result = endecoder.bin_to_obj(res.result_data.result_bin)
                    cache_access.set_result(result)
                    return
            spec_known = res.spec_known

class RemoteAssetException(Exception):
    pass

