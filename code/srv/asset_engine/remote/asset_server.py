import falcon.asgi as falcon_asgi
import uvicorn
import asyncio
import dataclasses
from srv.asset_engine.remote_tools.srv_tools import FalconRoute, StartupNotification
from srv.asset_engine.procs.procs_yn import ProcDir
from srv.asset_engine.remote_tools.remote_obj import RemoteObjHub
from srv.asset_engine.asset_engine_core.asset_method_defs import asset_endecoder
from functools import cached_property, partial
from typing import Any, Dict, Optional
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.tools.data.dataclass_utils import factory_field
from srv.asset_engine.remote.wire_data import AssetReply, WorkReply
from srv.tools.data.simple_queue import SimpleQueue
from srv.tools.timing import TimeoutWatcher
from srv.tools.data.uids import new_uid
from srv.asset_engine.remote.local_procs import start_local_worker

def asset_srv_main():
    ProcDir.set_pid0()
    
    proc_dir = ProcDir.from_env(endecoder=asset_endecoder)
    host, port, route = proc_dir.read_input()
    srv_url = f'http://{host}:{port}{route}'
    
    async def on_startup():
        proc_dir.write_output(srv_url)

    start_worker = partial(start_worker_on_srv, srv_url=srv_url)
    hub = RemoteObjHub(
        objects = dict(asset_srv=AssetSrv(start_worker=start_worker)),
        endecoder = asset_endecoder,
    )
    
    app  = falcon_asgi.App(
        middleware  = StartupNotification(run=on_startup),
        cors_enable = True,
    )
    app.add_route(route, FalconRoute(hub.bin_to_bin))
    uvicorn.run(app, host=host, port=port, log_level='warning')

@asset_class
class AssetState:
    hash: bytes
    comp_spec_bin:      Optional[bytes] = None
    comp_opts:          Optional[Any]   = None
    queued:             bool            = False
    future_result_data: asyncio.Future  = factory_field(asyncio.Future)

@asset_class
class AssetSrv:
    start_worker:       callable
    asset_states:       Dict[bytes, AssetState] = factory_field(dict)    
    asset_work_queues:  Dict                    = factory_field(dict)

    def asset_state_for(self, hash):
        if hash not in self.asset_states:
            self.asset_states[hash] = AssetState(hash=hash)
        return self.asset_states[hash]

    async def get_asset(self, account, hash, comp_spec_bin, comp_opts, timeout):
        asset_state = self.asset_state_for(hash=hash)
    
        if asset_state.comp_spec_bin is None and comp_spec_bin is not None:
            asset_state.comp_spec_bin = comp_spec_bin
            asset_state.comp_opts     = comp_opts
        spec_known = asset_state.comp_spec_bin is not None
    
        if spec_known and not asset_state.queued:
            await self.enqueue_work(WorkReply(
                hash=hash, 
                comp_spec_bin=asset_state.comp_spec_bin, 
                comp_opts=asset_state.comp_opts,
            ))
            asset_state.queued = True

        done, pending = await asyncio.wait([asset_state.future_result_data], timeout=timeout)
        result_data = await asset_state.future_result_data if done else None

        return AssetReply(spec_known=spec_known, result_data=result_data)

    async def get_work(self, hash, result_data, queue_id, worker_id, idle, timeout):
        got_result = False  # Maybe overridden below.
        if result_data is not None:
            asset_state = self.asset_state_for(hash)
            if not asset_state.future_result_data.done():
                asset_state.future_result_data.set_result(result_data)
            got_result = True

        work = await self.dequeue_work(
            queue_id=queue_id, worker_id=worker_id, 
            idle=idle, timeout=timeout,
        )
        return dataclasses.replace(work, got_result=got_result)

    async def enqueue_work(self, work):
        queue_id = 'local-queue'  # use work.comp_opts for dynamic queue_id.
        if queue_id not in self.asset_work_queues:
            queue = AssetWorkQueue(queue_id=queue_id, start_worker=self.start_worker)
            self.asset_work_queues[queue_id] = queue
        await self.asset_work_queues[queue_id].q_enqueue_work(work)

    async def dequeue_work(self, queue_id, worker_id, idle, timeout):
        if queue := self.asset_work_queues.get(queue_id, None):
            return await queue.q_dequeue_work(worker_id=worker_id, idle=idle, timeout=timeout)
        else:
            return WorkReply(stop_worker=True)

@asset_class
class AssetWorkQueue:
    queue_id:        str
    start_worker:    callable
    worker_watchers: dict     = factory_field(dict)

    @cached_property
    def work_queue(self):
        return SimpleQueue()

    async def q_enqueue_work(self, work):
        self.work_queue.put(work)
        await self.check_start_worker()

    async def q_dequeue_work(self, worker_id, idle, timeout):
        if watcher := self.worker_watchers.get(worker_id, None):
            watcher.expect_before(None, missing=None)
            try: 
                work = await self.work_queue.get(timeout=timeout)
                WORK_PROGRESS_TIMEOUT = 5 * 60
                watcher.expect_before(WORK_PROGRESS_TIMEOUT, missing='work-notice')
            except asyncio.TimeoutError:
                work = WorkReply(stop_worker=self.stop_worker(worker_id, idle))
                watcher.expect_before(1, missing='reconnect')
            return work
        else:
            return WorkReply(stop_worker=True)  # This worker is already removed.

    async def remove_worker(self, worker_id, missing):
        if missing is not None: print(f'remove_worker: {missing=} {worker_id=}')
        if watcher := self.worker_watchers.pop(worker_id, None):
            watcher.expect_before(None, missing=None)
        else:
            print(f'Warning: no such worker in remove_worker {missing=} {worker_id=}')

        await self.check_start_worker()

    async def check_start_worker(self):
        if not self.work_queue.empty() and not self.worker_watchers:
            worker_id = new_uid()
            STARTUP_TIMEOUT = 4*60
            watcher = TimeoutWatcher(fn=partial(self.remove_worker, worker_id=worker_id))
            watcher.expect_before(STARTUP_TIMEOUT, missing='startup')
            self.worker_watchers[worker_id] = watcher
            await self.start_worker(queue_id=self.queue_id, worker_id=worker_id)

    def stop_worker(self, worker_id, idle):
        if stop := (idle and self.work_queue.empty()):
            self.remove_worker(worker_id=worker_id, missing=None)
        return stop

async def start_worker_on_srv(srv_url, queue_id, worker_id):
    # For the time being: start all queues locally:
    await start_local_worker(srv_url=srv_url, queue_id=queue_id, worker_id=worker_id)

