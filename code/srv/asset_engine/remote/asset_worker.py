import time
from srv.asset_engine.procs.procs_yn import ProcDir
from srv.asset_engine.asset_engine_core.asset_method_defs import asset_endecoder
from srv.asset_engine.remote_tools.clients import ConnectionPool
from srv.asset_engine.remote_tools.remote_obj import RemoteObjProxy
from srv.asset_engine.remote.wire_data import ResultData

async def asset_worker_main():
    ProcDir.set_pid0()
    proc_dir = ProcDir.from_env(endecoder=asset_endecoder)
    [srv_url, queue_id, worker_id] = proc_dir.read_input()
    proc_dir.write_output(None)
    await run_asset_worker(srv_url=srv_url, queue_id=queue_id, worker_id=worker_id)
    print('Completed asset_worker_main')

async def run_asset_worker(srv_url, queue_id, worker_id):
    endecoder=asset_endecoder
    remotes = ConnectionPool(endecoder=endecoder)
    asset_srv = RemoteObjProxy(_remotes=remotes, _srv_url=srv_url, _key='asset_srv')
    timeout = asset_srv._remotes.default_timeout
    IDLE_TIMEOUT = 3
    last_work = time.time()
    hash = result_data = None
    
    while True:
        idle = last_work + IDLE_TIMEOUT < time.time()
        res = await asset_srv.get_work(
            hash=hash, result_data=result_data,
            queue_id=queue_id, worker_id=worker_id,
            idle=idle, timeout=timeout, 
        )
        if res.got_result:  hash = result_data = None
        if res.stop_worker: break
        if res.comp_spec_bin:
            hash        = res.hash
            comp_spec   = endecoder.bin_to_obj(res.comp_spec_bin)
            try:
                result  = await comp_spec.compute_spec()
                error   = None
            except Exception as exception:
                result  = None
                error   = f'{exception}'
            result_bin  = endecoder.obj_to_bin(result)
            result_data = ResultData(result_bin=result_bin, error=error)
            last_work   = time.time()
            
    await remotes.close_pool()

