"""
Create child processes --- python level.
"""
import asyncio
from typing import Any
from pathlib import Path
from dataclasses import dataclass
from srv.tools.timing import sleep

def set_pid(pids_dir, pid):
    """Maintain a unique pid file inside pids_dir"""
    if pids_dir is not None:
        pid = str(pid)
        pids_dir.mkdir(parents=True, exist_ok=True)
        pid_file = pids_dir/pid
        pid_file.write_text(pid)

async def run_proc_py(cmd_args=None, shell_cmd=None, cwd=None, env=None, pids_dir=None):
    if cwd: cwd = cwd.as_posix()
    if cmd_args is not None:
        proc_info = await asyncio.create_subprocess_exec(*cmd_args, cwd=cwd, env=env)
    else:
        proc_info = await asyncio.create_subprocess_shell(shell_cmd, cwd=cwd, env=env)
    
    set_pid(pids_dir=pids_dir, pid=proc_info.pid)
    return ProcOutput(proc_info=proc_info, pids_dir=pids_dir)

@dataclass
class ProcOutput:
    proc_info: Any
    pids_dir:  Path = None

    async def poll_res(self):
        returncode = self.proc_info.returncode
        if returncode is not None: rm_pid(pids_dir=self.pids_dir, pid=self.proc_info.pid)
        return returncode

    async def wait_proc(self, path=None, t1=0.01, factor=1.1):
        """Wait for process completion -- or not path.exists(): signal of result availability."""
        if path is not None:
            while await self.poll_res() is None and path.exists():
                await sleep(t1)
                t1 *= factor
        else:
            await self.proc_info.wait()
            await self.poll_res()          # Removes the pid file.
        return self

def rm_pid(pids_dir, pid):
    """Remove the unique pid file inside pids_dir"""
    if pids_dir is not None:
        pid = str(pid)
        (pids_dir/pid).unlink(missing_ok=True)

