"""
Higher-level child process functionality and conventions:
* Every Process has its own ProcDir for data
* We can store input and output data as encoded files.
* Compose process specifications from building blocks.
"""
import os
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from typing import Any, List, Sequence
from pathlib import Path
from srv.asset_engine.procs.procs_py import run_proc_py, set_pid
from functools import cached_property
from srv.tools.data.uids import new_uid
from srv.tools.filesystem.path_utils import rm_rf
from srv.tools.data.dataclass_utils import factory_field
from collections import defaultdict
from srv.tools.timing import sleep

@asset_class
class ExpectOutput:
    early: bool = False

    async def apply_to_proc(self, proc_env):
        io_dir = proc_env.proc_dir.io_dir
        (io_dir/'output_expected').write_text('1')
        if self.early: 
            wait_path = io_dir/'wait_for_output'
            wait_path.write_text('1')
            proc_env.add_spec(wait_path=wait_path)

@asset_class
class ProcDir:
    """This directory belongs to a sub process"""
    path:      Path
    endecoder: Any

    # os.environ[ProcDir.env_key]: The path to my proc data.
    env_key = 'YnProcDir'

    @classmethod
    def from_env(cls, endecoder):
        path = Path(os.environ.get(cls.env_key, '/mnt/Data/YnProcDir/Default/default/0'))
        return cls(path=path, endecoder=endecoder)  # ProcDir(...)

    @staticmethod
    def set_pid0(pid=None):
        if pid is None: pid = os.getpid()
        set_pid(pids_dir=ProcDir.from_env(endecoder=None).pids_dir, pid=pid)

    @cached_property
    def group(self):
        return self.path.parent.parent.name

    @cached_property
    def name(self):
        return self.path.parent.name

    def new_proc_dir(self, name, group=None, endecoder=None):
        group = group or self.group
        path  = self.path.parent.parent.parent/group/name/new_uid()
        path.mkdir(parents=True)
        return ProcDir(path, endecoder=endecoder or self.endecoder)

    def subdir(self, path):
        d = self.path/path
        d.mkdir(parents=True, exist_ok=True)
        return d

    @cached_property
    def code_dir(self):
        return self.subdir('code')

    @cached_property
    def pids_dir(self):
        return self.subdir('pid')

    @cached_property
    def io_dir(self):
        return self.subdir('io')

    def write_input(self, obj):
        input_path = self.io_dir/'input'
        input_path.write_bytes(self.endecoder.obj_to_bin(obj))

    def read_input(self):
        bin = (self.io_dir/'input').read_bytes()
        return self.endecoder.bin_to_obj(bin)

    def write_output(self, obj):
        output_path = self.io_dir/'output'
        assert (self.io_dir/'output_expected').exists(), f'No output expected: {output_path}'
        assert not output_path.exists(), f'Repeated output to: {output_path}'
        output_path.write_bytes(self.endecoder.obj_to_bin(obj))

        (self.io_dir/'wait_for_output').unlink(missing_ok=True)

    def read_output(self):
        if (self.io_dir/'output_expected').exists():
            bin = (self.io_dir/'output').read_bytes()
            return self.endecoder.bin_to_obj(bin)

    def remove_proc_dir(self):
        rm_rf(self.path)

@asset_class
class ProcInput:
    obj: Any

    async def apply_to_proc(self, proc_env):
        proc_env.proc_dir.write_input(self.obj)

def proc_config(**kwargs):
    return ProcConfig(kwargs=kwargs)

@asset_class
class ProcConfig:
    kwargs: dict

    async def apply_to_proc(self, proc_env):
        proc_env.add_spec(**self.kwargs)

async def run_proc(proc_dir, cmds):
    return await ProcEnv(proc_dir=proc_dir, cmds=cmds).run_proc()

@asset_class
class ProcEnv:
    """
    Mutable state machine to collect process specifications.
    """
    proc_dir:    ProcDir
    cmds:        List
    proc_output: Any      = factory_field()
    kvs:         dict     = factory_field(dict)
    lists:       dict     = factory_field(lambda: defaultdict(list))
    kv_keys   = 'wait_path immediate_result output_delay'.split()
    list_keys = 'python_snippets'.split()

    async def run_proc(self):
        await self.process_cmds()
    
        env  = os.environ.copy()
        env[self.proc_dir.env_key] = self.proc_dir.path.as_posix()
        pids_dir = self.proc_dir.pids_dir

        cwd = self.proc_dir.code_dir
    
        code = '\n'.join(c.strip() for c in self.lists['python_snippets'])
        (cwd / 'main_task.py').write_text(code)
        logs = self.proc_dir.subdir('logs')
        stdout, stderr = [logs / f for f in ('stdout.txt', 'stderr.txt')]
        shell_cmd = f"python -u main_task.py >{stdout} 2>{stderr}"
    
        self.proc_output = await run_proc_py(
            env=env, pids_dir=pids_dir, cwd=cwd, shell_cmd=shell_cmd, 
        )

        if 'immediate_result' in self.kvs: return self.kvs['immediate_result']
        await self.proc_output.wait_proc(self.kvs.get('wait_path', None))
        await sleep(self.kvs.get('output_delay', 0))
        return self.proc_dir.read_output()

    async def process_cmds(self):
        """Execute self.cmds to set up the process state."""
        cmds, self.cmds = self.cmds, []
        await self.apply_cmd(cmds)

    async def apply_cmd(self, cmd):
        """
        Recurse into lists and the output of cmds.
        """
        while cmd:
            if isinstance(cmd, Sequence):
                for sub_cmd in cmd:
                    await self.apply_cmd(sub_cmd)
                return
            else:
                cmd = await cmd.apply_to_proc(self)

    def add_spec(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.list_keys: 
                self.lists[key].extend(value)
            elif key in self.kv_keys: 
                assert key not in self.kvs, f'ProcEnv.set_value: -- repeated {key=}'
                self.kvs[key] = value
            else:
                raise ValueError(f'ProcEnv.add_spec: invalid {key=}')

