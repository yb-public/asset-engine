import dataclasses
from dataclasses import dataclass
import importlib
from srv.tools.data.dataclass_utils import factory_field
from typing import Any, Callable
from functools import cached_property

# Encoding registries -- and the classes that they are made up of are not serializable.
non_serializable_dataclass = dataclass

@non_serializable_dataclass
class EncodingRegistry:
    loader: callable = None
    
    _specs_list: list = factory_field(list)
    _specs_dict: dict = factory_field(dict)
    _push_to:    list = factory_field(list)

    def add_spec(self, enc_spec):
        d = self._specs_dict
        for key in enc_spec.registry_keys:
            if key in d: raise ValueError(f'EncodingRegistry conflict {key=}')
            d[key] = enc_spec
        
        self._specs_list.append(enc_spec)
        for registry2 in self._push_to:
            registry2.add_spec(enc_spec)

    def get_spec(self, reg_key):
        if res  := self._specs_dict.get(reg_key, None): return res
        if spec := reg_key.loadable and self.loader and self.loader(reg_key):
            self.add_spec(spec)
        if res  := self._specs_dict.get(reg_key, None): return res
        raise KeyError(reg_key)

    def push_specs_to(self, *registries):
        self._push_to.extend(registries)
        specs = self._specs_list.copy()
        for registry in registries:
            for spec in specs:
                registry.add_spec(spec)
        return self

    def pull_specs_from(self, *registries):
        for registry in registries:
            registry.push_specs_to(self)
        return self

@non_serializable_dataclass
class EncSpecAssetClass:
    cls: Callable

    @cached_property
    def registry_keys(self):
        return (
            RegistryKeyPy(module=self.cls.__module__, qualname=self.cls.__qualname__),
            RegistryKeyClass(cls=self.cls),
        )

    def decompose(self, obj):
        class_info  = [self.cls.__module__, self.cls.__qualname__]
        init_fields = [f.name for f in dataclasses.fields(self.cls)]
        attributes  = {k: getattr(obj, k) for k in init_fields}
        return class_info, attributes

    def recompose(self, attributes):
        return self.cls(**attributes)

@non_serializable_dataclass(frozen=True)
class RegistryKeyClass:
    cls: Any
    loadable = False

@non_serializable_dataclass(frozen=True)
class RegistryKeyPy:
    module:    str
    qualname:  str
    loadable = True

def py_loader(reg_key):
    importlib.import_module(reg_key.module)

