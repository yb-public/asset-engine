from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.asset_engine.asset_engine_core.asset_method_defs import asset_method

@asset_class
class Foo:
    x: int

    @asset_method
    async def bar(self, y):
        print(f'computing bar({y=}) ...')
        return Bar(x=self.x, y=y)

@asset_class
class Bar:
    x: int
    y: int

