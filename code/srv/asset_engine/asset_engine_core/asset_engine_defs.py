from typing import Any, Callable, Sequence
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.asset_engine.asset_engine_core.endecoding import basic_bin_to_hash
from srv.asset_engine.asset_engine_core.asset_engine_context import with_asset_engine
from srv.tools.data.dataclass_utils import factory_field
from functools import cached_property

@asset_class
class AssetEngine:
    cache_fns:   Sequence
    endecoder:   Any
    obj_to_hash: Callable = None
    bin_to_hash: Callable = basic_bin_to_hash
    account:     Any      = None

    def ctx(self):
        return with_asset_engine(self)

    async def compute_asset(self, comp_spec, comp_opts):
        cache_access = CacheAccess(asset_engine=self, comp_spec=comp_spec, comp_opts=comp_opts)
        for cache_fn in self.cache_fns:
            await cache_fn(cache_access)
            if cache_access.has_result:
                await cache_access.publish_result()
                return cache_access.result
        raise CannotComputeAssetException(f'{comp_spec=} {comp_opts=}')

@asset_class
class CacheAccess:
    asset_engine: Any
    comp_spec:    Any
    comp_opts:    Any
    has_result:   bool = factory_field(bool)
    result:       Any  = None
    _result_to:   list = factory_field(list) # Send the result to these functions.

    @cached_property
    def std_bin(self):
        return self.asset_engine.endecoder.obj_to_bin(self.comp_spec)

    @cached_property
    def std_hash(self):
        asset_engine = self.asset_engine
        if asset_engine.obj_to_hash: return asset_engine.obj_to_hash(self.comp_spec)
        else:                        return asset_engine.bin_to_hash(self.std_bin)

    def send_result_to(self, fn):
        self._result_to.append(fn)

    def set_result(self, result):
        self.result     = result
        self.has_result = True

    async def publish_result(self):
        if self.has_result:
            for fn in self._result_to:
                await fn(self.result)

class CannotComputeAssetException(Exception):
    pass

