from srv.tools.data.context_state import with_ctx_var
from contextvars import ContextVar

def with_asset_engine(asset_engine):
    return with_ctx_var(_asset_engine_ctx, asset_engine)

_asset_engine_ctx = ContextVar('_asset_engine_ctx', default=None)

def current_asset_engine():
    return _asset_engine_ctx.get() or _root_asset_engine_context[0]

_root_asset_engine_context = [None]

