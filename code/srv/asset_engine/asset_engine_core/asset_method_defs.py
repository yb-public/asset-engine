import inspect
from srv.tools.syntax_decorators import decorator_fn
from srv.asset_engine.asset_engine_core.asset_engine_context import current_asset_engine
from functools import wraps
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.asset_engine.asset_engine_core.endecoding import BasicEnDeCoder

@decorator_fn
def asset_method(fn, /):
    method = fn.__name__
    signature = inspect.signature(fn)
    @wraps(fn)
    async def proxy_fn(*args, **kwargs):
        bound     = signature.bind(*args, **kwargs)    # Normalize function arguments
        comp_spec = CompSpecMethod(method=method, args=bound.args, kwargs=bound.kwargs)
        comp_opts = None
        ce        = current_asset_engine()
        return await ce.compute_asset(comp_spec=comp_spec, comp_opts=comp_opts)
    proxy_fn.implementation = fn
    return proxy_fn

@asset_class
class CompSpecMethod:
    method: str
    args:   list
    kwargs: dict

    async def compute_spec(self):
        obj = self.args[0]
        return await getattr(obj, self.method).implementation(*self.args, **self.kwargs)

asset_endecoder = BasicEnDeCoder(registry=asset_class.registry)

