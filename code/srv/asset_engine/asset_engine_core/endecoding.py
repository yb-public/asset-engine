import hashlib
import msgpack_yomo
from srv.asset_engine.asset_engine_core.encoding_registry import EncodingRegistry, RegistryKeyClass, RegistryKeyPy
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from dataclasses import field

def basic_bin_to_hash(bin):
    return hashlib.blake2b(bin).digest()

@asset_class
class BasicEnDeCoder:
    registry: EncodingRegistry = field(repr=False)

    def obj_to_bin(self, obj):
        def pack(obj):
            return msgpack_yomo.packb(obj, default=hook)
    
        def hook(data):  # Capture context `self` in closure.
            reg_key    = RegistryKeyClass(cls=data.__class__)
            spec       = self.registry.get_spec(reg_key)
            decomposed = spec.decompose(data)
            bin        = msgpack_yomo.ExtType(code=1, data=pack(decomposed))
            return bin

        return pack(obj)

    def bin_to_obj(self, bin):
        def unpack(packed):
            return msgpack_yomo.unpackb(
               packed, ext_hook=hook, use_list=False, strict_map_key=False,
            )
        
        def hook(code, packed):  # Capture context `self` in closure.
            if code == 1:
                (module, qualname), attributes = unpack(packed)
                reg_key = RegistryKeyPy(module=module, qualname=qualname)
                spec    = self.registry.get_spec(reg_key)
                return spec.recompose(attributes)
            else: raise ValueError(f'bin_to_obj: hook {code=} not yet supported.')

        return unpack(bin)

