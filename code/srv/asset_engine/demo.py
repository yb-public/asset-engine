import asyncio
from srv.asset_engine.asset_engine_core.asset_engine_defs import AssetEngine
from srv.asset_engine.remote_tools.clients import ConnectionPool
from srv.asset_engine.asset_engine_core.test_asset_engine import Foo
from srv.asset_engine.remote.cache import RemoteAssets
from srv.asset_engine.remote_tools.remote_obj import RemoteObjProxy
from srv.asset_engine.asset_engine_core.asset_method_defs import asset_endecoder
from srv.asset_engine.remote.local_procs import start_local_asset_srv
from srv.tools.timing import timing_ctx

async def asset_engine_demo():
    remotes = ConnectionPool(endecoder=asset_endecoder)
    srv_url = await start_local_asset_srv(
        host='0.0.0.0', port=1234, route='/asset_srv',
    )
    asset_srv = RemoteObjProxy(_remotes=remotes, _srv_url=srv_url, _key='asset_srv')
    cache_fns = [RemoteAssets(asset_srv=asset_srv).cache_fn]
    asset_engine = AssetEngine(cache_fns=cache_fns, endecoder=asset_endecoder)

    with asset_engine.ctx():
        foo = Foo(1)
        with timing_ctx('---> compute-1'):
            print(await foo.bar(2))
        with timing_ctx('---> compute-2'):
            print(repr(await foo.bar(2)))
        with timing_ctx('---> compute-3'):
            print((await foo.bar(2)).y)

    await remotes.close_pool()
    
if __name__ == '__main__':
    asyncio.run(asset_engine_demo())

