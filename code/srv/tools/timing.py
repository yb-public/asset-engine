import asyncio
from asyncio import create_task
import time
from typing import Optional
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.tools.data.dataclass_utils import factory_field
from functools import partial
from contextlib import contextmanager

sleep = asyncio.sleep

@asset_class
class TimeoutWatcher:
    fn: callable
    fut: asyncio.Future = factory_field(asyncio.Future)
    handler: Optional[asyncio.TimerHandle] = None

    def expect_before1(self, timeout, /, *args, **kwargs):
        if self.handler: self.handler.cancel()
        callback=partial(self.fn, *args, **kwargs)
        timeout += 0.0001
        self.handler = asyncio.get_running_loop().call_later(delay=timeout, callback=callback)

    def expect_before(self, timeout, /, *args, **kwargs):
        self.fut.set_result(None)
        self.fut = fut = asyncio.Future()
        if timeout is not None:
            async def expect():
                done, pending = await asyncio.wait([fut], timeout=timeout)
                if pending and (awaitable := self.fn(*args, **kwargs)):
                    await awaitable
            create_task(expect())

@contextmanager
def timing_ctx(msg):
    t0 = time.time()
    try: yield None
    finally: print(f'{msg}: {round(time.time() - t0, 3)}s')

