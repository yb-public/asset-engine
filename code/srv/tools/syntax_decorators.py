from functools import wraps

def decorator_fn(handler=None, /, **kwargs):
    """
    Flexible decorator factory -- kwargs and exposure.

    @decorator_fn
    def my_decorator(fn, /, a=None, b=None):
        @wraps(fn):
        def decorated(*args, **kwargs):
            print(f'{a=} {b=} {args=} {kwargs=}'')
        return decorated

    specific_decorator      = my_decorator(a=1)        # Partially specify kwargs...
    more_specific_decorator = specific_decorator(b=2)  # Do this iteratively...
    # You can access the partially specified arguments in the decorators:
    print(specific_decorator.a, more_specific_decorator.a, more_specific_decorator.b)

    # Choose just one of the following decorators:
    @my_decorator
    @my_decorator(a=1)
    @my_decorator(a=1, b=2)
    @specific_decorator
    @specific_decorator(b=2)
    @more_specific_decorator
    def foo(x, y): return x + y

    foo(11, 22)  # Prints kwargs and args passed.
    """
    kwargs1 = kwargs
    
    @wraps(handler)
    def decorator(fn=None, /, **kwargs):
        handler2, fn2 = (handler, fn) if handler is not None else (fn, None)
        
        if fn2 is not None: return handler(fn2, **kwargs1, **kwargs)
        else:               return decorator_fn(handler2, **kwargs1, **kwargs)

    for key in kwargs1:
        setattr(decorator, key, kwargs1[key])
    return decorator

