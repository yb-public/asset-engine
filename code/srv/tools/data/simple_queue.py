import asyncio
import time
from typing import Optional
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from dataclasses import field

@asset_class
class SimpleQueue:
    queue:  list                     = field(default_factory=list, repr=False, init=False)
    future: Optional[asyncio.Future] = field(default=None, repr=False, init=False)

    def put(self, item):
        self.queue.append(item)
        if self.future is not None:
            old_future, self.future = self.future, asyncio.Future()
            old_future.set_result(None)

    async def get(self, timeout=None, default=None):
        t0 = time.time()
        time_limit = time.time() + timeout 
        while len(self.queue) == 0:
            if timeout is None:
                dt = None
            else:
                dt = timeout - (time.time() - t0)
            if self.future is None:
                self.future = asyncio.Future()
            done, pending = await asyncio.wait([self.future], timeout=dt)
            if not done:
                return default
        return self.queue.pop(0)

    def empty(self):
        return len(self.queue) == 0

