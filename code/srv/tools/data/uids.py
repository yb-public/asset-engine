import time

def new_uid(uid=None):
    return uid or str(time.time_ns())

