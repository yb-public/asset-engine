from contextlib import contextmanager

@contextmanager
def with_ctx_var(ctx_var, value):
    try:
        token = ctx_var.set(value)
        yield value
    finally:
        ctx_var.reset(token)

