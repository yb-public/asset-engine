import asyncio
import traceback as TraceBack
import sys
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from pathlib import Path
from srv.tools.data.io_helpers import print_list

@asset_class
class CatchExceptions:
    info:     dict = None
    error:    str  = None
    trace:    list = None

    def __enter__(self):
        return self

    def __exit__(self, type, exc, tb):
        if exc: self.report_exception(exc, tb)
        if type is KeyboardInterrupt: return False
        if type is asyncio.exceptions.CancelledError: return False
        return True  # Catch the exception

    def report_exception(self, exc, tb=None):
        if exc is None: return
        x_name = exc.__class__.__name__
    
        self.error = f'{x_name}: {exc}\n{self.info or {}}'
    
        print(f'=== Error {self.info or {}} {x_name}: {exc}')
        self.trace = [
            f'--- {Path(frame.filename).name} {frame.lineno}:\n{frame.line}'
            for frame in 
                reversed(TraceBack.extract_tb(tb or sys.exc_info()[2]))
        ]
        print_list(self.trace)

