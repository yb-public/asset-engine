import shutil
from pathlib import Path

def rm_rf(path, mkdir=False):
    path = Path(path).expanduser()
    if mkdir:
        path.mkdir(parents=True, exist_ok=True)
        for f in path.iterdir():
            rm_rf(f)
    elif path.exists():
        if path.is_dir():
            shutil.rmtree(path.as_posix())
        else:
            path.unlink()
    return path

