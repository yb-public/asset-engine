from srv.asset_engine.distributed.dl_demo import dl_demo

async def run_task_fn():
    goals = [['macro:local_demo']]
    for specs in goals: await dl_demo(specs=specs, netfree=True)
    print('Done')

