import inspect

def exactly_one(items):
    assert len(items) == 1, f'exactly_one failed for: {items}'
    return items[0]

async def dl_transform(dl, *transformations, copy=True):
    if copy: dl = {key: list(value) for key, value in dl.items()}
    t_prefix, dl_prefix = 't_', 'dl_'
    for transformation in transformations:
        if _specialize_ := getattr(transformation, '_specialize_', None):
            res = _specialize_(dl)
            if inspect.isawaitable(res): res = await res
            transformation = res
        for method in transformation.__class__.__dict__.keys():
            if method.startswith(t_prefix):
                k = method.removeprefix(t_prefix)
                res = getattr(transformation, method)(dl.pop(k, []))
            elif method.startswith(dl_prefix):
                res = getattr(transformation, method)(dl)
            else:
                res = ()
            if inspect.isawaitable(res): res = await res
            if inspect.isasyncgen(res):  res = [r async for r in res]
            for key, value in res or ():            
                if key not in dl: dl[key] = []
                dl[key].append(value)
            for transformations2 in dl.pop('InsertTransformations', ()):
                await dl_transform(dl, *transformations2, copy=False)
    return dl

