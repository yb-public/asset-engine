import asyncio
import sys
from srv.asset_engine.distributed.dl_trafos import CallRc, DLRunContext, DLSpecs, MkDockerRef, RcMacros, SelectRcImplementation, SelectRcInput, ShowRcResults, VerifyRc
from srv.asset_engine.distributed.dl_tools import dl_transform

async def dl_demo(specs, netfree=False):
    netfree = bool(netfree and netfree not in ('0', 'False'))
    if not specs: specs = ['macro:remote_demo']
    await dl_transform(
        {},
        DLSpecs(specs=specs),
        RcMacros(),
        DLRunContext(netfree=netfree, debug=True),
        MkDockerRef(),
        SelectRcInput(),
        SelectRcImplementation(),
        VerifyRc(),
        CallRc(),
        ShowRcResults(),
    )
if __name__ == '__main__':
    _netfree = sys.argv[1:] and sys.argv[1]
    _specs   = sys.argv[2:]
    asyncio.run(dl_demo(specs=_specs, netfree=_netfree))

