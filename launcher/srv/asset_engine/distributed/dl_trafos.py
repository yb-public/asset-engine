import asyncio
import json
import time
import dataclasses
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.asset_engine.distributed.dl_tools import exactly_one
from pathlib import Path
from srv.asset_engine.procs.procs_yn import ProcDir
from srv.tools.filesystem.path_utils import rm_rf
from typing import Sequence
from textwrap import dedent
from functools import cached_property
from dirhash import dirhash
from srv.asset_engine.procs.procs_py import run_proc_py
from collections import defaultdict
from shlex import quote as shell_quote

@asset_class
class CallRc:
    pass

    async def dl_run_rc_cmd(self, dl):
        parts = dl.pop(f'rc_cmd_args', ())
        cmd_args = [arg for part in parts for arg in part]
        if dl.get('no_run_rc_cmd'): print(*cmd_args); return
        run_dir = exactly_one(dl['base_dir']) / f'run-{time.time()}'
        run_dir.mkdir(parents=True, exist_ok=True)  
        (run_dir/'cmd.json').write_text(json.dumps(cmd_args)+'\n')
        (run_dir/'cmd.txt' ).write_text(' '.join(f"'{x}'" for x in cmd_args)+'\n')
        stdout, stderr = [run_dir/f for f in ('stdout', 'stderr')]
        with stdout.open('wb') as out, stderr.open('wb') as err:
            p = await asyncio.create_subprocess_exec(*cmd_args,stdout=out,stderr=err)
            error = await p.wait()
            if  error or dl.pop('show_run_rc_cmd_output', None):
                print(*[f"'{arg}'" for arg in cmd_args])
                print(stdout.read_text().rstrip())
                print(stderr.read_text().rstrip())
            if error:
                raise Exception('run_rc_cmd failed.')

@asset_class
class DLRunContext:
    netfree: bool = False
    debug:   bool = False

    def dl_apply(self, dl):
        if self.netfree: yield 'netfree', True
        if self.debug:
            if self.netfree: base_dir = '/mnt/Data/DLTmp'
            else:            base_dir = '~/DLTmp'
        else:                base_dir = ProcDir.from_env(endecoder=None).path/'DLTmp'
        base_dir = Path(base_dir).expanduser()
        if dl.pop('clear_DLTmp', None): rm_rf(base_dir)
        base_dir.mkdir(parents=True, exist_ok=True)
        yield 'base_dir', base_dir

@asset_class
class DLSpecs:
    specs: Sequence = ()

    def dl_apply(self, _):
        yield from expand_dl_specs(self.specs)

def expand_dl_specs(specs):
    if isinstance(specs, str): specs = [specs]
    for spec1 in specs:
        if isinstance(spec1, (tuple, list)): yield spec1
        elif isinstance(spec1, str):
            for spec2 in spec1.split():
                if ':' in spec2: yield spec2.split(':', 1)
                else:            yield spec2, ''
        elif spec1:
            raise ValueError(f'Tuple or str expected for expand_dl_specs: {spec1}')

@asset_class
class MkDockerRef:
    # These members will be set by _specialize_(dl):
    netfree: bool = False
    docker_dir: Path = None

    def _specialize_(self, dl):
        netfree = bool(dl.get('netfree', None))
        docker_dir = exactly_one(dl['base_dir']) / f'docker-{time.time()}'
        docker_dir.mkdir(parents=True, exist_ok=True)  
        return dataclasses.replace(self, netfree=netfree, docker_dir=docker_dir)

    def t_docker_from(self, images):
        yield 'Dockerfile_root', f'From {exactly_one(images)}'

    def dl_create_app_user(self, _):
        cmd = 'RUN adduser -u 1000 --disabled-login --shell /bin/bash --gecos "" app_user'
        yield 'Dockerfile_root', cmd

    def t_docker_io(self, enabled):
        if enabled:
            yield 'Dockerfile_root', 'RUN addgroup --gid 127 docker'
            yield 'Dockerfile_root', 'RUN usermod -aG docker app_user'
            yield 'apt', 'docker.io'

    def t_apt(self, packages):
        if packages:
            pkgs = ' '.join(sorted(set(packages)))
            yield 'Dockerfile_root', 'RUN apt-get update'
            yield 'Dockerfile_root', f'RUN apt-get -y install {pkgs}'

    def t_netfree_certs(self, enabled):
        certificate = dedent("""
        -----BEGIN CERTIFICATE-----
        MIIDzzCCAregAwIBAgIJANJjV3HuLlVqMA0GCSqGSIb3DQEBCwUAMH4xCzAJBgNV
        BAYTAklMMQ8wDQYDVQQIDAZpc3JhZWwxEjAQBgNVBAcMCWplcnVzYWxlbTEQMA4G
        A1UECgwHTmV0RnJlZTEZMBcGA1UECwwQbmV0ZnJlZS42MTNtLm9yZzEdMBsGA1UE
        AwwUTmV0RnJlZSBTaWduICxSTCBJU1AwHhcNMTQxMTE1MjMxNjU0WhcNMjQwOTIz
        MjMxNjU0WjB+MQswCQYDVQQGEwJJTDEPMA0GA1UECAwGaXNyYWVsMRIwEAYDVQQH
        DAlqZXJ1c2FsZW0xEDAOBgNVBAoMB05ldEZyZWUxGTAXBgNVBAsMEG5ldGZyZWUu
        NjEzbS5vcmcxHTAbBgNVBAMMFE5ldEZyZWUgU2lnbiAsUkwgSVNQMIIBIjANBgkq
        hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2gEv5S0YJFYrWSNJJNhtBRN9IfyFhh/l
        NCo2K8DKkq3CFCthHADZu1v9gRAa9NxC7wrsmK/wBTwoVwcHKrxrmxt/vs6bwZZ7
        9/TwKDA9Ar3SY6bvsDvij0/53frRoAhUJwdFkwlkIN0DNJWv8n3xAoIRBI3pOxcE
        890Q2RDw+ydc4OIgETIel3vDqe7aTXTjb7BJjqIYTf9Wad3gmTGpRToNhC0kyQuk
        MN2XxYHhK/+jjoL/O9vcd3ZLaYlYbQbT/Ud63CfIyvB+ASkqnRGFBIQttkzVrPHP
        fRkhM+C6BlRfv0njt9Hl5jJjreWXSryd5iSwVVU6VtHo6TZFtUwA/wIDAQABo1Aw
        TjAdBgNVHQ4EFgQUG9R/R+XGIIaXGs74B2+JNqoka7kwHwYDVR0jBBgwFoAUG9R/
        R+XGIIaXGs74B2+JNqoka7kwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOC
        AQEAQh9Id5A0LvTu4yCqM8rrP1mpEBKv4XjMWihR9gZ6fBY8NkyWBfyQkylc6Jlm
        vrtFH4etuQkElN7tL1ZAyXAmOVnXB19asft/swA+UGEpdqmdIJiz+Q9KSP4c64Xw
        L9svsOqBouiBYPWpURO0AJVk42lmXVGopGcet8hzMmmBejlCbDWi7mu+RJEMIMkp
        g6cITc6m/Hrawt3oRxfyERKdoXUpTtTV5Cu4oHLwgcdMbp7IvHJO1SS5fP8OhObN
        9aVsHN+TzDsXZqkvYn/Tn4fO4YtUdokCSzc39caq9ZiwqUpD0dSOYDotlIIXmzxE
        OmymXu+SCGzKmR72vZouU0NuTw==
        -----END CERTIFICATE-----
        """).strip()
        commands = dedent(r"""
        RUN mkdir -p "/usr/share/ca-certificates/extra/"
        COPY netfree-ca.crt /usr/share/ca-certificates/extra/netfree-ca.crt
        RUN dpkg-reconfigure -f noninteractive ca-certificates
        RUN sed -i.bak  s/\!extra\\/netfree-ca/extra\\/netfree-ca/g /etc/ca-certificates.conf
        RUN dpkg-reconfigure -f noninteractive ca-certificates
        """)
        if enabled and self.netfree:
            yield 'file', ('netfree-ca.crt', certificate)
            yield 'Dockerfile_root', commands

    def dl_switch_to_app_user(self, _):
        pass

    def t_start_feature(self, _):
        start_container = ['#!/usr/bin/env python3']
        for key in 'args auto_delete src run start'.split():
            code = getattr(self, f'feature_{key}')
            start_container.append(dedent(code).strip()+'\n')
        yield 'file', ['start_container.py', start_container]
        yield 'Dockerfile_user', 'COPY --chown=app_user start_container.py start_container.py'
        yield 'Dockerfile_user', 'RUN chmod u+x start_container.py'
        yield 'start_cmd', './start_container.py'

    feature_args = """
        import json, sys
        start_args = json.loads(sys.argv[1])
    """

    feature_auto_delete = """
        from pathlib import Path

        def fn_auto_delete(enabled=True):
            print(f'auto_delete: {enabled=}')
            marker = Path('/tmp/autodelete-marker')
            autodelete = marker.exists()
            marker.write_text('1')
            if enabled and autodelete:
                import subprocess
                from urllib.request import urlopen, Request as urllib_Request
                def metadata(key):
                    url = 'http://metadata.google.internal/computeMetadata/v1/instance/'+key
                    headers = {'Metadata-Flavor': 'Google'}
                    with urlopen(urllib_Request(url=url, headers=headers)) as response:
                        return response.read().decode()
                zone = metadata('zone')
                name = metadata('name')
                cmd  = [
                    'gcloud', 'compute', 'instances', 'delete', name, '--zone', zone,
                    '--delete-disks=all', '--quiet',
                ]
                p = subprocess.run(cmd)
                if p.returncode: raise Exception('gcloud failed.')

    """

    feature_src = """
        import itertools, json, subprocess, sys
        from pathlib import Path

        git_dir = Path('/home/app_user/git/')
        src_dir = Path('/home/app_user/src/')

        git_counter = itertools.count()
        def fn_src(git, branch='main', dir='.'):
            repo = Path(git).stem + f'-g{next(git_counter)}'
            cmd = [
                'git', 'clone', '--depth', '1', git,
                '--single-branch', '--branch', branch,
                repo,
            ]
            if not (git_dir/repo).exists():
                p = subprocess.run(cmd, cwd=git_dir)
                if p.returncode: raise Exception('git failed.')
                (src_dir/repo).symlink_to((git_dir/repo/dir).resolve())

        start_args['fn_load_src'] = [[[],{}]]
        entry_points = {}
        def fn_load_src():
            for src in sorted(src_dir.iterdir()):
                src = src.resolve()
                sys.path.insert(0, src.as_posix())
                entry_points_json = src / 'entry_points.json'
                if entry_points_json.exists():
                    for k, v in json.loads(entry_points_json.read_text()).items():
                        entry_points[k] = v
    """

    feature_run = """
        import importlib, inspect

        def fn_run(cmd, *args, **kwargs):
            assert cmd in entry_points, f'Unknown entry_point: {cmd}'
            module_name, fname = entry_points[cmd].split(':', 1)
            module = importlib.import_module(module_name)
            fn = getattr(module, fname)
            res = fn(*args, **kwargs)
            if inspect.isawaitable(res):
                import asyncio
                asyncio.run(res)
    """

    feature_start = """
        for fn in (fn_auto_delete, fn_src, fn_load_src, fn_run):
            for args, kwargs in start_args.pop(fn.__name__, ()):
                fn(*args, **kwargs)
        if start_args:
            print(f'Ignored start_args: {dict(start_args)}')
    """

    @cached_property
    def trusted_hosts(self):
        hosts = 'pypi.org files.pythonhosted.org gitlab.com'.split() if self.netfree else ()
        return ''.join(f' --trusted-host {host}' for host in hosts)

    def t_pip(self, lines):
        if lines:
            hosts = self.trusted_hosts
            yield 'file', ('requirements.txt', lines)
            yield 'Dockerfile_user', 'COPY requirements.txt requirements.txt'
            yield 'Dockerfile_user', f'RUN pip install --user -r requirements.txt{hosts}'

    def t_msgpack_yomo(self, enabled):
        if enabled:
            hosts = self.trusted_hosts
            yield 'Dockerfile_user', f'RUN pip install --user Cython~=0.29.13{hosts}'
            yield 'Dockerfile_user', f'RUN pip install --user https://gitlab.com/yb-public/msgpack_yomo/-/raw/main/msgpack_yomo.zip{hosts}'

    def t_install_gcloud(self, enabled):
        if enabled and not self.netfree:
            yield 'Dockerfile_user', 'RUN curl -sSL https://sdk.cloud.google.com | bash'
            yield 'Dockerfile_user', 'ENV PATH $PATH:/home/app_user/google-cloud-sdk/bin'

    def t_expose_ports(self, ports):
        if ports:
            yield 'Dockerfile_user', f'EXPOSE {" ".join(sorted(set(ports)))}'

    def t_Dockerfile_root(self, lines):
        yield 'Dockerfile_all', lines

    def t_Dockerfile_user(self, lines):
        yield 'Dockerfile_all', [
            'USER app_user',
            'WORKDIR /home/app_user',
            'RUN mkdir src git',
            'ENV PATH="/home/app_user/.local/bin:$PATH"',
        ]
        yield 'Dockerfile_all', lines

    def t_Dockerfile_all(self, parts):
        yield 'file', ('Dockerfile', [line for part in parts for line in part])

    def t_file(self, specs):
        for file_name, content in specs:
            if isinstance(content, (tuple, list)):
                content = ''.join(line+'\n' for line in content)
            (self.docker_dir/file_name).write_text(content)
        return ()

    def t_docker_image(self, image_refs):
        image_ref = exactly_one(image_refs)
        if ':' not in image_ref:
            image_ref += f':{dirhash(self.docker_dir, "sha256")}'
        yield 'docker_image', image_ref

    async def dl_docker_build(self, dl):
        cwd = self.docker_dir
        tag = exactly_one(dl['docker_image'])
        shell_cmd = f'docker build . --tag {tag} >output.txt 2>&1'
        p = await run_proc_py(shell_cmd=shell_cmd, cwd=cwd)
        await p.wait_proc()
        if await p.poll_res():
            print((cwd/'Dockerfile').read_text())
            print((cwd/'output.txt').read_text())
            raise Exception('docker build failed')

    async def dl_t_docker_push(self, dl):
        if enabled := dl.pop('docker_push', None):
            cwd = self.docker_dir
            tag = exactly_one(dl['docker_image'])
            shell_cmd = f'docker push {tag} >output-push.txt 2>&1'
            p = await run_proc_py(shell_cmd=shell_cmd, cwd=cwd)
            await p.wait_proc()
            if await p.poll_res():
                print((cwd/'output-push.txt').read_text())
                raise Exception('docker push failed')

@asset_class
class RcMacros:
    pass

    def expand_macro(self, macro):
        specs = getattr(self, f'macro_{macro}', None)
        assert specs is not None, f'Undefined macro: {macro}'
        for k, v in expand_dl_specs(specs):
            if k == 'macro': yield from self.expand_macro(v)
            else:            yield k, v

    def t_macro(self, macros):
        for macro in macros:
            yield from self.expand_macro(macro)

    macro_small_demo = """
        clear_DLTmp
        docker_image:us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/demo
        docker_from:python:3.9.6-buster
        docker_io:1 docker_in_docker:1 pip:dirhash==0.2.1 msgpack_yomo:1
        netfree_certs:1
        start_arg:run:=demo_srv,=1,=2,hello=world
        macro:verify_hide_std
        show_run_rc_cmd_output:1
    """
    macro_local_demo = """
        macro:small_demo
        use_local_src:/mnt/Data/git/asset-engine/srv
        rc_implementation:LocalDockerRc
        no_run_rc_cmd-off:1
    """
    macro_remote_demo = """
        macro:small_demo
        start_arg_off:auto_delete:enabled=1 install_gcloud:1
        start_arg:src:git=https://gitlab.com/yb-public/asset-engine.git,branch=main,dir=srv
        rc_implementation:GceUbuntuRc
        docker_push:1
    """
    macro_local_git_demo = """
        macro:small_demo
        start_arg:src:git=https://gitlab.com/yb-public/asset-engine.git,branch=main,dir=srv
        rc_implementation:LocalDockerRc
    """
    not_used = """
        rc_implementation:GceContainerRc
    """

    macro_verify_hide_std = """
        verify_hide:verify_hide verify_hide:netfree verify_hide:base_dir
        verify_hide:rc_cmd_args verify_hide:show_run_rc_cmd_output
    """

@asset_class
class SelectRcImplementation:
    pass

    def t_rc_implementation(self, implementations):
        implementation = exactly_one(implementations)
        if implementation == 'LocalDockerRc':
            yield 'InsertTransformations', [LocalDockerRc()]
        elif implementation == 'GceUbuntuRc':
            yield 'InsertTransformations', [GceUbuntuRc()]
        elif implementation == 'GceContainerRc':
            yield 'InsertTransformations', [GceContainerRc()]
        else:
            raise ValueError(f'Invalid rc_implementation: {implementation}')

@asset_class
class GceContainerRc:
    pass

    def dl_create_with_container(self, _):
        instance_id = f'asset-demo-{int(time.time())}'
        yield 'rc_cmd_args', [
            'gcloud', 'compute', 'instances', 
            'create-with-container', instance_id, 
            '--boot-disk-device-name', instance_id,
            '--boot-disk-size=10GB', '--boot-disk-type=pd-balanced', 
        ]
        yield 'rc_cmd_args', [
            '--project=fastai-255512', '--zone=us-central1-a', 
            '--network-interface=network-tier=PREMIUM,subnet=default',
            '--service-account=844561511675-compute@developer.gserviceaccount.com',
            '--scopes=https://www.googleapis.com/auth/cloud-platform',
            '--no-shielded-secure-boot', '--shielded-vtpm', '--shielded-integrity-monitoring', 
            '--tags=http-server,https-server', 

            '--container-restart-policy=always', '--container-privileged', 
            '--no-restart-on-failure', '--maintenance-policy=TERMINATE',
            '--provisioning-model=SPOT', '--instance-termination-action=DELETE',
    
            '--image=projects/cos-cloud/global/images/cos-stable-97-16919-29-40',
            '--labels=container-vm=cos-stable-97-16919-29-40',
        ]

    def t_docker_in_docker(self, enabled):
        if enabled:
            yield 'mount_src_target', ['/var/run/docker.sock', '/var/run/docker.sock']

    def t_mount_src_target(self, pairs):
        mode = 'rw'  # or 'ro'
        for src, target in pairs:
            yield 'rc_cmd_args', [
                '--container-mount-host-path',
                f'host-path={src},mode={mode},mount-path={target}'
            ]

    def t_docker_image(self, images):
        yield 'rc_cmd_args', ['--container-image', exactly_one(images)]

    def t_start_cmd(self, cmds):
        cmd = exactly_one(cmds)
        if isinstance(cmd, str): cmd = cmd.split(',')
        command, *args = cmd
        yield 'rc_cmd_args', ['--container-command', command]
        self.t_start_arg(args)

    def t_start_arg(self, args):
        yield 'rc_cmd_args', ['--container-arg', [start_args_to_json_str(args)]]

def start_args_to_json_str(specs):
    start_args = defaultdict(list)
    for spec in specs:
        if ':' in spec:
            args, kwargs = [], {}
            cmd, args_str = spec.split(':', 1)
            for arg in args_str.split(','):
                if '=' in arg and not arg.startswith('='):
                    k, v = arg.split('=', 1)
                    kwargs[k] = v
                else:
                    args.append(arg.removeprefix('='))
            start_args[f'fn_{cmd}'].append((args, kwargs))
        else:
            print(f'ignore invalid start_arg: {spec}')
    return json.dumps(start_args)

@asset_class
class GceUbuntuRc:
    data_dir: Path = None  # overridden by _specialize_

    def _specialize_(self, dl):
        data_dir = exactly_one(dl['base_dir']) / f'gce-ubunutu-{time.time()}'
        data_dir.mkdir(parents=True, exist_ok=True)  
        return dataclasses.replace(self, data_dir=data_dir)

    def dl_create_with_ubuntu(self, _):
        instance_id = f'asset-demo-{int(time.time())}'
        os_image = 'projects/fastai-255512/global/images/asset-engine-1'
        disk_type = 'projects/fastai-255512/zones/us-central1-a/diskTypes/pd-balanced'
        disk_spec = f'--create-disk=auto-delete=yes,boot=yes,device-name={instance_id},image={os_image},mode=rw,size=10,type={disk_type}'
        machine_type = 'e2-standard-2'  # 'e2-micro'
        yield 'rc_cmd_args', [
            'gcloud', 'compute', 'instances', 'create', instance_id, disk_spec,
            '--machine-type', machine_type,
        ]
        yield 'rc_cmd_args', [
            '--project=fastai-255512', '--zone=us-central1-a',
            '--network-interface=network-tier=PREMIUM,subnet=default',
            '--service-account=844561511675-compute@developer.gserviceaccount.com',
            '--scopes=https://www.googleapis.com/auth/cloud-platform',
            '--tags=http-server,https-server',
            '--maintenance-policy=TERMINATE', '--provisioning-model=SPOT', '--preemptible',
            '--instance-termination-action=DELETE', '--no-restart-on-failure',
            '--no-shielded-secure-boot', '--shielded-vtpm', '--shielded-integrity-monitoring',
            '--reservation-affinity=any',
        ]
        # --metadata=init_gpu=0,cmd_args='python code/srv/demo.py'

    def dl_startup_script(self, dl):
        startup_script = self.data_dir / 'startup.sh'
        docker_image = exactly_one(dl.pop('docker_image'))
        docker_socks = '--volume=/var/run/docker.sock:/var/run/docker.sock'
        meta1        = 'http://metadata.google.internal/computeMetadata/v1/instance'
        meta2        = "-H 'Metadata-Flavor: Google'"
        del_opts     = '--delete-disks=all --quiet'
        start_cmd    = shell_quote(exactly_one(dl.pop('start_cmd')))
        start_arg    = shell_quote(start_args_to_json_str(dl.pop('start_arg', ())))
        docker_args  = f'{start_cmd} {start_arg}'
        output       = '/tmp/docker-output'
        output_base  = 'gs://yaakovnet-net-asset-engine/logs/output-'
        docker_opts  = '--restart=always' or '--rm'  # Not used yet
        code = dedent(f"""
            sudo chmod a+rw /var/run/docker.sock
            sudo gcloud auth configure-docker us-central1-docker.pkg.dev --quiet
            sudo docker pull {docker_image}
            sudo docker run {docker_socks} {docker_image} {docker_args} >{output} 2>&1
            gsutil cp {output} {output_base}$(date --utc +%y-%m-%d--%R-%N)
            export ZONE=$(curl {meta1}/zone {meta2})
            export NAME=$(curl {meta1}/name {meta2})
            gcloud compute instances delete ${{NAME}} --zone=${{ZONE}} {del_opts}
            sudo shutdown -h now
        """).strip()
        startup_script.write_text(code)
        yield 'rc_cmd_args', [f'--metadata-from-file=startup-script={startup_script}']

@asset_class
class LocalDockerRc:
    pass

    def t_use_local_src(self, srcs):
        for n, src in enumerate(srcs):
            src_name = f'{Path(src).name}-{n}'
            yield 'mount_src_target', [src, f'/home/app_user/src/{src_name}']

    def t_docker_in_docker(self, enabled):
        if enabled:
            yield 'mount_src_target', ['/var/run/docker.sock', '/var/run/docker.sock']

    def dl_docker_run(self, _):
        yield 'rc_cmd_args', ['docker', 'run']

    def t_mount_src_target(self, pairs):
        for src, target in pairs:
            yield 'docker_run_opts', [f'--volume={src}:{target}']

    def t_docker_run_opts (self, parts):
        yield 'rc_cmd_args', [opt for part in parts for opt in part]

    def t_docker_image(self, images):
        yield 'rc_cmd_args', [exactly_one(images)]

    def t_start_cmd(self, cmds):
        cmd = exactly_one(cmds)
        if isinstance(cmd, str): cmd = cmd.split(',')
        yield 'rc_cmd_args', cmd

    def t_start_arg(self, args):
        yield 'rc_cmd_args', [start_args_to_json_str(args)]

@asset_class
class SelectRcInput:
    pass

@asset_class
class ShowRcResults:
    pass

@asset_class
class VerifyRc:
    pass

    def dl_show(self, dl):
        hide = set(dl.get('verify_hide', ()))
        show = [(k,v) for k, v in dl.items() if k not in hide]
        if show:
            print(f'=== VerifyRc: unknown keys:')
            for k, v in show: 
                print(f'{k}: {v}')

