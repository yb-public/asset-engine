from srv.asset_engine.asset_engine_core.encoding_registry import EncSpecAssetClass, EncodingRegistry, py_loader
from srv.tools.syntax_decorators import decorator_fn
from dataclasses import dataclass

@decorator_fn
def asset_class_factory(cls, /, registry=None, frozen=False):
    cls = dataclass(frozen=frozen)(cls)
    registry.add_spec(EncSpecAssetClass(cls=cls))
    return cls

asset_class = asset_class_factory(registry=EncodingRegistry(loader=py_loader))

