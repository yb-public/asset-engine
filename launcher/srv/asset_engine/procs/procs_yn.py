"""
Higher-level child process functionality and conventions:
* Every Process has its own ProcDir for data
* We can store input and output data as encoded files.
* Compose process specifications from building blocks.
"""
import os
from typing import Any
from pathlib import Path
from srv.asset_engine.asset_engine_core.asset_class_defs import asset_class
from srv.asset_engine.procs.procs_py import set_pid
from functools import cached_property
from srv.tools.data.uids import new_uid
from srv.tools.filesystem.path_utils import rm_rf

@asset_class
class ProcDir:
    """This directory belongs to a sub process"""
    path:      Path
    endecoder: Any

    # os.environ[ProcDir.env_key]: The path to my proc data.
    env_key = 'YnProcDir'

    @classmethod
    def from_env(cls, endecoder):
        path = Path(os.environ.get(cls.env_key, '/mnt/Data/YnProcDir/Default/default/0'))
        return cls(path=path, endecoder=endecoder)  # ProcDir(...)

    @staticmethod
    def set_pid0(pid=None):
        if pid is None: pid = os.getpid()
        set_pid(pids_dir=ProcDir.from_env(endecoder=None).pids_dir, pid=pid)

    @cached_property
    def group(self):
        return self.path.parent.parent.name

    @cached_property
    def name(self):
        return self.path.parent.name

    def new_proc_dir(self, name, group=None, endecoder=None):
        group = group or self.group
        path  = self.path.parent.parent.parent/group/name/new_uid()
        path.mkdir(parents=True)
        return ProcDir(path, endecoder=endecoder or self.endecoder)

    def subdir(self, path):
        d = self.path/path
        d.mkdir(parents=True, exist_ok=True)
        return d

    @cached_property
    def code_dir(self):
        return self.subdir('code')

    @cached_property
    def pids_dir(self):
        return self.subdir('pid')

    @cached_property
    def io_dir(self):
        return self.subdir('io')

    def write_input(self, obj):
        input_path = self.io_dir/'input'
        input_path.write_bytes(self.endecoder.obj_to_bin(obj))

    def read_input(self):
        bin = (self.io_dir/'input').read_bytes()
        return self.endecoder.bin_to_obj(bin)

    def write_output(self, obj):
        output_path = self.io_dir/'output'
        assert (self.io_dir/'output_expected').exists(), f'No output expected: {output_path}'
        assert not output_path.exists(), f'Repeated output to: {output_path}'
        output_path.write_bytes(self.endecoder.obj_to_bin(obj))

        (self.io_dir/'wait_for_output').unlink(missing_ok=True)

    def read_output(self):
        if (self.io_dir/'output_expected').exists():
            bin = (self.io_dir/'output').read_bytes()
            return self.endecoder.bin_to_obj(bin)

    def remove_proc_dir(self):
        rm_rf(self.path)

