from dataclasses import field

def factory_field(factory=None):
    factory = factory or (lambda: None)
    return field(default_factory=factory, repr=False, init=False)

