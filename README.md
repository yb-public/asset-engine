== Git: asset-engine
Store the generated asset-engine code to: /mnt/Data/git/asset-engine/code/
git clone https://gitlab.com/yb-public/asset-engine.git

== Create cusom image: ubuntu ubuntu-os-pro-cloud/global/images/ubuntu-pro-2204-jammy-v20220506
* Install docker with `sudo apt-get update && sudo apt-get --yes install docker.io && sudo apt clean`
* Save as image: asset-engine-img

== Docker registry:
https://cloud.google.com/artifact-registry/docs/docker/store-docker-container-images

Create the docker repository:
gcloud artifacts repositories create asset-engine-docker-repo --repository-format=docker \
  --location=us-central1 --description="Docker repository for the Asset Engine"

Set up authentication:
gcloud auth configure-docker us-central1-docker.pkg.dev --quiet

Pull, tag and push an image:
docker pull us-docker.pkg.dev/google-samples/containers/gke/hello-app:1.0

docker tag us-docker.pkg.dev/google-samples/containers/gke/hello-app:1.0 \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/quickstart-image:tag1

docker push \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/quickstart-image:tag1

docker pull \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/quickstart-image:tag1

gcloud artifacts repositories list
gcloud artifacts docker images list \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo


== Metadata
gcloud compute instances create ... --metadata=key1=value1,key2=value2
export KEY1=$(curl http://metadata.google.internal/computeMetadata/v1/instance/attributes/key1 -H "Metadata-Flavor: Google" -f)

Browse all metadata:
curl "http://metadata.google.internal/computeMetadata/v1/instance/?recursive=true" -H "Metadata-Flavor: Google"


Todo:
* build a simple docker image
* deploy the container, check the output
* ...
