* Need to remove pre-compiled output: msgpack_yomo/_cmsgpack.cpp
* Need to install Cython~=0.29.13 separately before being able to compile
  ==> Can we declare build dependencies in python packages?
* Automatization: make sure to re-zip the code when changed.
-----
Check:
* Can requirements.txt refer to a zip file via http:// URLs?
* Can we declare build dependencies such as Cython~=0.29.13 for a package?
-----
* Changes:
  * Sorted dictionary keys for stable hash functions (for sync and async)
  * async encoding and async decoding for endecodex.
-----
* Download and unpack: msgpack-1.0.2.tar.gz
mg msgpack-1.0.2 msgpack-yomo

* Create pipenv and requirements.txt:

Cython~=0.29.13
/home/yaakov/MsgPackYomo/msgpack-yomo.zip

* cd into the msgpack-1.0.2 source directory.

mv msgpack/ msgpack_yomo
rm msgpack_yomo/_cmsgpack.cpp

Makefile     -- msgpack/ -> msgpack_yomo/
MANIFEST.in  -- msgpack  -> msgpack_yomo
setup.py     -- msgpack  -> msgpack_yomo

msgpack_yomo/unpack_define.h -- msgpack/sysdep.h -> msgpack_yomo/sysdep.h

msgpack_yomo/_packer.pyx:
elif PyDict_CheckExact(o): ... for k, v in sorted(d.items()):
elif not strict_types and PyDict_Check(o):


rm -f ../msgpack_yomo.zip; zip -r ../msgpack_yomo.zip *
pip uninstall msgpack_yomo; pip install ../msgpack_yomo.zip --trusted-host pypi.org --trusted-host files.pythonhosted.org

import msgpack_yomo
a = [1,2,3]
b = msgpack_yomo.packb(a)
c = msgpack_yomo.unpackb(b)
print(c)

python test.py
