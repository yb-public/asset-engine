#!/bin/sh
cd /root/

#export init_gpu=$(curl http://metadata.google.internal/computeMetadata/v1/instance/attributes/init_gpu -H "Metadata-Flavor: Google" -f)
#if [[ "$init_gpu" == "1" ]]; then sudo cos-extensions install gpu; fi

echo git > /tmp/01-git
git clone https://gitlab.com/yb-public/asset-engine.git Data
chmod a+rwx Data

echo auth > /tmp/02-auth
sudo gcloud auth configure-docker us-central1-docker.pkg.dev --quiet

echo cmd_args > /tmp/03-cmd_args

export cmd_args=$( \
  curl http://metadata.google.internal/computeMetadata/v1/instance/attributes/cmd_args \
  -H "Metadata-Flavor: Google" -f \
)

echo pull > /tmp/05-pull
sudo docker pull \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/asset-all

echo run > /tmp/04-run
sudo docker run --volume=$(pwd)/Data:/mnt/Data \
  us-central1-docker.pkg.dev/fastai-255512/asset-engine-docker-repo/asset-all \
  $cmd_args > docker-output 2>&1

echo log > /tmp/05-log
gsutil cp docker-output \
  gs://yaakovnet-net-asset-engine/logs/docker-output-$(date --utc +%y-%m-%d--%R-%N)

echo zone > /tmp/06-delete
export ZONE=$(curl http://metadata.google.internal/computeMetadata/v1/instance/zone \
  -H 'Metadata-Flavor: Google'
)
export NAME=$(curl http://metadata.google.internal/computeMetadata/v1/instance/name \
  -H 'Metadata-Flavor: Google'
)

# Container OS: Run compute delete on the current instance. Need to run in a container
# because COS machines don't come with gcloud installed
# docker run --entrypoint "gcloud" google/cloud-sdk:alpine compute instances delete ${NAME} --delete-disks=all --zone=${ZONE}

gcloud compute instances delete ${NAME} --delete-disks=all --zone=${ZONE} --quiet

echo shutdown > /tmp/07-shutdown
sudo shutdown -h now

echo end > /tmp/08-end


